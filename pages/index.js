import Header from '../components/header'
import Footer from '../components/footer'
import Post0 from '../components/post0'
import Post1 from '../components/post1'
import Post2 from '../components/post2'

export default function IndexPage() {
  return (
    <div>
      <Header />
<div class="container px-3 bg-gray-300 bg-opacity-100 py-3">
            <section>
<Post0 />
<Post1 />



  
</section>
</div>
<Footer />

    </div>
  )
}