import Link from 'next/link'

export default function Footer() {
  return (
    <>

    <footer class="justify-center text-gray-300 bg-teal-600">
        <div class="container py-3">
            <h1 class="text-center text-lg lg:text-2xl">
     পশ্চিমবঙ্গ 
            </h1>

            <h1 class="text-center text-lg -mt-2 lg:text-2xl">
   দক্ষিণ ২৪ পরগনা 
            </h1>

            <h1 class="text-center lg:text-2xl">
   Email: alap.sc@criptext.com
            </h1>
</div>
    </footer>
    </>
  )
}
