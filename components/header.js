import Link from 'next/link'

export default function Header() {
  return (
    <>

    <header class="justify-center text-gray-300 bg-teal-600">
        <div class="container py-3 shadow-lg">
            <h1 class="text-center text-lg lg:text-2xl">
                বিজ্ঞান পরিচয়
            </h1>

            <h1 class="text-center lg:text-2xl">
     ব্রেকথ্রু সায়েন্স সোসাইটি'র সাথে যুক্ত বিজ্ঞান ক্লাব
            </h1>
</div>

                
   <ul class="flex px-4 items-center  justify-between mt-2 shadow-lg">
      <li>
        <Link href="/">
          <a className="font-bold">আলাপ</a>
        </Link>
      </li>
      <li>
        <Link href="/about">
          <a>পরিচয়</a>
        </Link>
      </li>

<li>
        <Link href="/member">
          <a>সদস্যবৃন্দ</a>
        </Link>
      </li>
      <li>
        <Link href="/archive">
          <a>সংগ্রহশালা</a>
        </Link>
      </li>
    </ul>
                    
            
    
    </header>
    </>
  )
}
