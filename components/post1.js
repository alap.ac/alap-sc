import Link from 'next/link'

export default function Post1() {
  return (
    <>

    <div class="max-w-2xl mx-auto px-4 mb-3 py-4 bg-white rounded-lg shadow-md">
        <div class="flex justify-between items-center">
            <span class="font-light text-gray-600 text-sm">৭ই আগস্ট, ২০২০</span>
            <a class="px-3 py-1 bg-gray-600 text-gray-100 text-sm font-bold rounded hover:bg-gray-500">কমিটি</a>
        </div>

        <div class="mt-2">
            <a href="#" class="text-2xl text-gray-700 font-bold hover:text-gray-600 hover:underline">সদস্য হওয়ার জন্য আবেদন। </a>
            <p class="mt-2 text-gray-600">সদস্য হওয়ার জন্য  সকলকে সাদর আবেদন জানাচ্ছি। </p>
        </div>
        
        <div class="flex justify-between items-center mt-4">
            <a href="#" class="text-blue-600 hover:underline"></a>

            <div class="flex items-center">
              
                <a class="text-gray-700 font-bold cursor-pointer"></a>
            </div>
        </div>
    </div>

    
    </>
  )
}