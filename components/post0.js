import Link from 'next/link'

export default function Post0() {
  return (
    <>

    
    <div class="max-w-2xl mx-auto px-4 mb-3 py-4 bg-white rounded-lg shadow-md">
        <div class="flex justify-between items-center">
            <span class="font-light text-gray-600 text-sm">২৮শে ফেব্রুয়ারী, ২০২০</span>
            <a class="px-3 py-1 bg-gray-600 text-gray-100 text-sm font-bold rounded hover:bg-gray-500">ক্লাব</a>
        </div>

        <div class="mt-2">
            <a href="#" class="text-2xl text-gray-700 font-bold hover:text-gray-600 hover:underline">আনুষ্ঠানিক ক্লাব প্রতিষ্ঠা</a>
            <p class="mt-2 text-gray-600">২৮শে ফেব্রুয়ারী, ২০২০ তারিখে 'জাতীয় বিজ্ঞান দিবস'-এ কয়েকজন শিক্ষক ও ছাত্রছাত্রীদের উপস্থিতিতে দক্ষিণ ২৪ পরগনা জেলার রাধাকান্তপুর অঞ্চলে "আলাপ" বিজ্ঞান ক্লাব আনুষ্ঠানিকভাবে প্রতিষ্ঠা করা হয়। ব্রেকথ্রু সায়েন্স সোসাইটি'র পক্ষ থেকে উপস্থিত ছিলেন কিংশুক হালদার, সম্পাদক, দক্ষিণ ২৪ পরগনা জেলা কমিটি। ক্লাবের 'মটো' অর্থাৎ সাধারণ আদর্শ "বিজ্ঞান পরিচয়" ।</p>
        </div>
        
        <div class="flex justify-between items-center mt-4">
            <a href="#" class="text-blue-600 hover:underline"></a>

            <div class="flex items-center">
              
                <a class="text-gray-700 font-bold cursor-pointer"></a>
            </div>
        </div>
    </div>

    
    </>
  )
}
